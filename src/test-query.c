/* -*- mode: C; c-basic-offset: 4 -*- */

#define _BSD_SOURCE
#include <stdio.h>
#include <fcntl.h>
#include <rpmlib.h>
#include <rpmdb.h>
#include <gtk/gtk.h>
#include "rpm-info.h"

static void
activate_uri(RpmInfo *info, const gchar *uri)
{
    g_message("uri activated: %s", uri);
}

int
main(int argc, char **argv)
{
    rpmdb db;
    rpmdbMatchIterator mi;
    Header header;
    GtkWidget *window, *rpminfo;

    gtk_init(&argc, &argv);
    rpmReadConfigFiles(NULL, NULL);

    if (argc != 2) {
	fprintf(stderr, "usage: test-query rpmname\n");
	return 1;
    }

    if (rpmdbOpen(NULL, &db, O_RDONLY, 0644)) {
	fprintf(stderr, "could not open RPM database\n");
	return 1;
    }
    mi = rpmdbInitIterator(db, RPMDBI_LABEL, argv[1], 0);
    if (!mi) {
	fprintf(stderr, "could not find any matches\n");
	return 1;
    }
    header = rpmdbNextIterator(mi);
    if (!header) {
	fprintf(stderr, "could not find any matches\n");
	return 1;
    }

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "RPM Info");
    g_signal_connect(G_OBJECT(window), "destroy",
		     G_CALLBACK(gtk_main_quit), NULL);

    rpminfo = rpm_info_new();
    rpm_info_set_header(RPM_INFO(rpminfo), header);
    g_signal_connect(G_OBJECT(rpminfo), "activate_uri",
		     G_CALLBACK(activate_uri), NULL);

    rpmdbFreeIterator(mi);
    rpmdbClose(db);

    gtk_container_add(GTK_CONTAINER(window), rpminfo);
    gtk_widget_show(rpminfo);

    gtk_widget_show(window);

    gtk_main();

    return 0;
}
