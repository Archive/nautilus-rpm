/* -*- mode: C; c-basic-offset: 4 -*- */
#ifndef RPM_INFO_H
#define RPM_INFO_H

#include <rpmlib.h>
#include <gtk/gtk.h>

#define RPM_TYPE_INFO            (rpm_info_get_type ())
#define RPM_INFO(obj)            (GTK_CHECK_CAST ((obj), RPM_TYPE_INFO, RpmInfo))
#define RPM_INFO_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), RPM_TYPE_INFO, RpmInfoClass))
#define RPM_IS_INFO(obj)         (GTK_CHECK_TYPE ((obj), RPM_TYPE_INFO))
#define RPM_IS_INFO_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), RPM_TYPE_INFO))

typedef struct _RpmInfo      RpmInfo;
typedef struct _RpmInfoClass RpmInfoClass;

struct _RpmInfo {
    GtkVBox parent;

    GtkWidget *name;
    GtkWidget *notebook;

    /* info page */
    GtkWidget *icon;
    GtkWidget *summary;
    GtkWidget *group;
    GtkWidget *size;
    GtkWidget *vendor;
    GtkWidget *packager;
    GtkWidget *url;
    GtkWidget *installdate;
    GtkTextBuffer *description;

    /* files page */
    GtkTreeStore *files;

    /* changelog page */
    GtkTextBuffer *changelog;
    

    /* requires/provides/conflicts */
    GtkTreeStore *requires;
    GtkTreeStore *provides;
    GtkTreeStore *conflicts;
    GtkTreeStore *obsoletes;
};

struct _RpmInfoClass {
    GtkVBoxClass parent_class;

    void (*activate_uri) (RpmInfo *self, const gchar *uri);
};

GType rpm_info_get_type  (void);
GtkWidget *rpm_info_new  (void);
void rpm_info_clear      (RpmInfo *self);
void rpm_info_set_header (RpmInfo *self, Header header);

#endif
