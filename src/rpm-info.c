/* -*- mode: C; c-basic-offset: 4 -*- */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#define _BSD_SOURCE
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>

#ifdef ENABLE_NLS
#  include <libintl.h>
#  define _(s) dgettext(GETTEXT_PACKAGE, s)
#  ifdef gettext_noop
#    define N_(s) gettext_noop(s)
#  else
#    define N_(s) (s)
#  endif
#else
#  define textdomain(s) (s)
#  define bindtextdomain(domain,dir) (domain)
#  define _(s) (s)
#  define N_(s) (s)
#endif

#include "rpm-info.h"
#include <rpmdb.h>
#include <libgnomevfs/gnome-vfs-utils.h>

#define FILENAME_COLUMN 0
#define FILETYPE_COLUMN 1

#define RESOURCE_COLUMN 0
#define COLOUR_COLUMN 1
#define URI_COLUMN 2
#define HAS_EXPANDED_COLUMN 3

#define INFO_PAGE 0
#define FILES_PAGE 1
#define CHANGELOG_PAGE 2
#define REQUIRES_PAGE 3
#define PROVIDES_PAGE 4
#define CONFLICTS_PAGE 5
#define OBSOLETES_PAGE 6

enum {
    ACTIVATE_URI,
    LAST_SIGNAL
};

static void rpm_info_init       (RpmInfo *info);
static void rpm_info_class_init (RpmInfoClass *class);

GType
rpm_info_get_type(void)
{
    static GType object_type = 0;

    if (!object_type) {
        static const GTypeInfo object_info = {
            sizeof(RpmInfoClass),
            (GBaseInitFunc) NULL,
            (GBaseFinalizeFunc) NULL,
            (GClassInitFunc) rpm_info_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof(RpmInfo),
            0, /* n_preallocs */
            (GInstanceInitFunc) rpm_info_init,
        };
        object_type = g_type_register_static(GTK_TYPE_VBOX,
                                             "RpmInfo",
                                             &object_info, 0);
    }

    return object_type;
}

static void rpm_info_destroy(GtkObject *object);

GObjectClass *parent_class;
static guint rpm_info_signals[LAST_SIGNAL] = { 0 };

static void
rpm_info_class_init(RpmInfoClass *class)
{
    GObjectClass *object_class;
    GtkObjectClass *gtkobject_class;

    parent_class = g_type_class_peek_parent(class);
    object_class = G_OBJECT_CLASS(class);
    gtkobject_class = GTK_OBJECT_CLASS(class);

    gtkobject_class->destroy = rpm_info_destroy;

    rpm_info_signals[ACTIVATE_URI] =
	g_signal_new ("activate_uri",
		      G_OBJECT_CLASS_TYPE(class),
		      G_SIGNAL_RUN_FIRST,
		      G_STRUCT_OFFSET(RpmInfoClass, activate_uri),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__STRING,
		      G_TYPE_NONE, 1,
		      G_TYPE_STRING);
}

static void row_activated_cb(GtkTreeView *tree_view, GtkTreePath *path,
			     GtkTreeViewColumn *column, RpmInfo *self);
static gboolean resource_test_expand_row(GtkTreeView *view, GtkTreeIter *iter,
					 GtkTreePath *path);

static GtkWidget *
make_bold_label(const gchar *message)
{
    gchar *string;
    GtkWidget *label;

    string = g_strconcat("<b>", message, "</b>", NULL);
    label = gtk_label_new(string);
    g_free(string);

    gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

    return label;
}

static void
rpm_info_init(RpmInfo *self)
{
    GtkWidget *label, *table, *box, *swin, *text, *tree;

    gtk_widget_push_composite_child();

    gtk_box_set_spacing(GTK_BOX(self), 3);

    self->name = gtk_label_new(NULL);
    gtk_label_set_use_markup(GTK_LABEL(self->name), TRUE);
    gtk_label_set_selectable(GTK_LABEL(self->name), TRUE);
    gtk_box_pack_start(GTK_BOX(self), self->name, FALSE, TRUE, 0);
    gtk_widget_show(self->name);

    self->notebook = gtk_notebook_new();
    gtk_box_pack_start(GTK_BOX(self), self->notebook, TRUE, TRUE, 0);
    gtk_widget_show(self->notebook);

    label = gtk_label_new(_("Info"));
    table = gtk_table_new(8, 3, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 3);
    gtk_table_set_col_spacings(GTK_TABLE(table), 3);
    gtk_container_set_border_width(GTK_CONTAINER(table), 3);
    gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook), table, label);
    gtk_widget_show(label);
    gtk_widget_show(table);

    self->icon = gtk_image_new();
    gtk_misc_set_alignment(GTK_MISC(self->icon), 0.5, 0.0);
    gtk_misc_set_padding(GTK_MISC(self->icon), 3, 3);
    gtk_table_attach(GTK_TABLE(table), self->icon, 0,1, 0,7,
		     GTK_FILL, GTK_FILL, 0, 0);
    gtk_widget_show(self->icon);

    label = make_bold_label(_("Summary:"));
    gtk_table_attach(GTK_TABLE(table), label, 1,2, 0,1,
		     GTK_FILL, GTK_FILL, 0, 0);
    gtk_widget_show(label);

    self->summary = gtk_label_new("");
    gtk_label_set_selectable(GTK_LABEL(self->summary), TRUE);
    gtk_label_set_line_wrap(GTK_LABEL(self->summary), TRUE);
    gtk_misc_set_alignment(GTK_MISC(self->summary), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), self->summary, 2,3, 0,1,
		     GTK_FILL|GTK_EXPAND|GTK_SHRINK, GTK_FILL, 0, 0);
    gtk_widget_show(self->summary);

    label = make_bold_label(_("Group:"));
    gtk_table_attach(GTK_TABLE(table), label, 1,2, 1,2,
		     GTK_FILL, GTK_FILL, 0, 0);
    gtk_widget_show(label);

    self->group = gtk_label_new("");
    gtk_label_set_selectable(GTK_LABEL(self->group), TRUE);
    gtk_misc_set_alignment(GTK_MISC(self->group), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), self->group, 2,3, 1,2,
		     GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
    gtk_widget_show(self->group);

    label = make_bold_label(_("Size:"));
    gtk_table_attach(GTK_TABLE(table), label, 1,2, 2,3,
		     GTK_FILL, GTK_FILL, 0, 0);
    gtk_widget_show(label);

    self->size = gtk_label_new("");
    gtk_label_set_selectable(GTK_LABEL(self->size), TRUE);
    gtk_misc_set_alignment(GTK_MISC(self->size), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), self->size, 2,3, 2,3,
		     GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
    gtk_widget_show(self->size);

    label = make_bold_label(_("Vendor:"));
    gtk_table_attach(GTK_TABLE(table), label, 1,2, 3,4,
		     GTK_FILL, GTK_FILL, 0, 0);
    gtk_widget_show(label);

    self->vendor = gtk_label_new("");
    gtk_label_set_selectable(GTK_LABEL(self->vendor), TRUE);
    gtk_misc_set_alignment(GTK_MISC(self->vendor), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), self->vendor, 2,3, 3,4,
		     GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
    gtk_widget_show(self->vendor);

    label = make_bold_label(_("Packager:"));
    gtk_table_attach(GTK_TABLE(table), label, 1,2, 4,5,
		     GTK_FILL, GTK_FILL, 0, 0);
    gtk_widget_show(label);

    self->packager = gtk_label_new("");
    gtk_label_set_selectable(GTK_LABEL(self->packager), TRUE);
    gtk_misc_set_alignment(GTK_MISC(self->packager), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), self->packager, 2,3, 4,5,
		     GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
    gtk_widget_show(self->packager);

    label = make_bold_label(_("URL:"));
    gtk_table_attach(GTK_TABLE(table), label, 1,2, 5,6,
		     GTK_FILL, GTK_FILL, 0, 0);
    gtk_widget_show(label);

    self->url = gtk_label_new("");
    gtk_label_set_selectable(GTK_LABEL(self->url), TRUE);
    gtk_misc_set_alignment(GTK_MISC(self->url), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), self->url, 2,3, 5,6,
		     GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
    gtk_widget_show(self->url);

    label = make_bold_label(_("Install date:"));
    gtk_table_attach(GTK_TABLE(table), label, 1,2, 6,7,
		     GTK_FILL, GTK_FILL, 0, 0);
    gtk_widget_show(label);

    self->installdate = gtk_label_new("");
    gtk_label_set_selectable(GTK_LABEL(self->installdate), TRUE);
    gtk_misc_set_alignment(GTK_MISC(self->installdate), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), self->installdate, 2,3, 6,7,
		     GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0);
    gtk_widget_show(self->installdate);

    swin = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(swin),
					GTK_SHADOW_IN);
    gtk_table_attach(GTK_TABLE(table), swin, 0,3, 7,8,
		     GTK_FILL|GTK_EXPAND, GTK_FILL|GTK_EXPAND, 0, 0);
    gtk_widget_show(swin);
    
    text = gtk_text_view_new();
    self->description = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text));
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(text), GTK_WRAP_WORD);
    gtk_text_view_set_editable(GTK_TEXT_VIEW(text), FALSE);
    gtk_container_add(GTK_CONTAINER(swin), text);
    gtk_widget_show(text);

    label = gtk_label_new(_("Files"));
    swin = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(swin),
					GTK_SHADOW_IN);
    gtk_container_set_border_width(GTK_CONTAINER(swin), 3);
    gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook), swin, label);
    gtk_widget_show(label);
    gtk_widget_show(swin);

    self->files = gtk_tree_store_new(3, G_TYPE_STRING, G_TYPE_STRING,
				     G_TYPE_STRING);

    tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(self->files));
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(tree),
						-1, _("File name"),
						gtk_cell_renderer_text_new(),
						"text", FILENAME_COLUMN,
						NULL);
    g_signal_connect_object(tree, "row_activated",
			    G_CALLBACK(row_activated_cb), self, 0);

    gtk_container_add(GTK_CONTAINER(swin), tree);
    gtk_widget_show(tree);

    label = gtk_label_new(_("Change log"));
    swin = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(swin),
					GTK_SHADOW_IN);
    gtk_container_set_border_width(GTK_CONTAINER(swin), 3);
    gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook), swin, label);
    gtk_widget_show(label);
    gtk_widget_show(swin);

    text = gtk_text_view_new();
    self->changelog = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text));
    gtk_text_view_set_editable(GTK_TEXT_VIEW(text), FALSE);
    gtk_text_buffer_create_tag(self->changelog, "date",
			       "weight", PANGO_WEIGHT_BOLD,
			       NULL);
    gtk_text_buffer_create_tag(self->changelog, "name",
			       NULL);
    gtk_text_buffer_create_tag(self->changelog, "text",
			       "left_margin", 20,
			       NULL);
    gtk_container_add(GTK_CONTAINER(swin), text);
    gtk_widget_show(text);

    label = gtk_label_new(_("Requires"));
    box = gtk_vbox_new(FALSE, 5);
    gtk_container_set_border_width(GTK_CONTAINER(box), 3);
    gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook), box, label);
    gtk_widget_show(label);
    gtk_widget_show(box);

    label = make_bold_label(_("Resources required by this package:"));
    gtk_box_pack_start(GTK_BOX(box), label, FALSE, TRUE, 0);
    gtk_widget_show(label);

    swin = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(swin),
					GTK_SHADOW_IN);
    gtk_box_pack_start(GTK_BOX(box), swin, TRUE, TRUE, 0);
    gtk_widget_show(swin);

    self->requires = gtk_tree_store_new(4, G_TYPE_STRING, G_TYPE_STRING,
					G_TYPE_STRING, G_TYPE_BOOLEAN);
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(self->requires),
					 RESOURCE_COLUMN, GTK_SORT_ASCENDING);
    g_object_set_data(G_OBJECT(self->requires), "match_type",
		      GINT_TO_POINTER(RPMTAG_PROVIDENAME));
    g_object_set_data(G_OBJECT(self->requires), "want_matches",
		      GINT_TO_POINTER(TRUE));

    tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(self->requires));
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(tree),
						-1, _("Resource"),
						gtk_cell_renderer_text_new(),
						"text", RESOURCE_COLUMN,
						"foreground", COLOUR_COLUMN,
						NULL);
    gtk_tree_view_column_set_sort_column_id (
		gtk_tree_view_get_column (GTK_TREE_VIEW (tree), 0),
		RESOURCE_COLUMN);
    gtk_tree_view_set_enable_search(GTK_TREE_VIEW(tree), TRUE);
    gtk_tree_view_set_search_column(GTK_TREE_VIEW(tree), RESOURCE_COLUMN);
    g_signal_connect_object(tree, "row_activated",
			    G_CALLBACK(row_activated_cb), self, 0);
    g_signal_connect(tree, "test_expand_row",
		     G_CALLBACK(resource_test_expand_row), NULL);

    gtk_container_add(GTK_CONTAINER(swin), tree);
    gtk_widget_show(tree);

    label = gtk_label_new(_("Provides"));
    box = gtk_vbox_new(FALSE, 5);
    gtk_container_set_border_width(GTK_CONTAINER(box), 3);
    gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook), box, label);
    gtk_widget_show(label);
    gtk_widget_show(box);

    label = make_bold_label(_("Resources provided by this package:"));
    gtk_box_pack_start(GTK_BOX(box), label, FALSE, TRUE, 0);
    gtk_widget_show(label);

    swin = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(swin),
					GTK_SHADOW_IN);
    gtk_box_pack_start(GTK_BOX(box), swin, TRUE, TRUE, 0);
    gtk_widget_show(swin);

    self->provides = gtk_tree_store_new(4, G_TYPE_STRING, G_TYPE_STRING,
					G_TYPE_STRING, G_TYPE_BOOLEAN);
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(self->provides),
					 RESOURCE_COLUMN, GTK_SORT_ASCENDING);
    g_object_set_data(G_OBJECT(self->provides), "match_type",
		      GINT_TO_POINTER(RPMTAG_REQUIRENAME));
    g_object_set_data(G_OBJECT(self->provides), "want_matches",
		      GINT_TO_POINTER(TRUE));

    tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(self->provides));
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(tree),
						-1, _("Resource"),
						gtk_cell_renderer_text_new(),
						"text", RESOURCE_COLUMN,
						"foreground", COLOUR_COLUMN,
						NULL);
    gtk_tree_view_column_set_sort_column_id (
		gtk_tree_view_get_column (GTK_TREE_VIEW (tree), 0),
		RESOURCE_COLUMN);
    gtk_tree_view_set_enable_search(GTK_TREE_VIEW(tree), TRUE);
    gtk_tree_view_set_search_column(GTK_TREE_VIEW(tree), RESOURCE_COLUMN);
    g_signal_connect_object(tree, "row_activated",
			    G_CALLBACK(row_activated_cb), self, 0);
    g_signal_connect(tree, "test_expand_row",
		     G_CALLBACK(resource_test_expand_row), NULL);

    gtk_container_add(GTK_CONTAINER(swin), tree);
    gtk_widget_show(tree);

    label = gtk_label_new(_("Conflicts"));
    box = gtk_vbox_new(FALSE, 5);
    gtk_container_set_border_width(GTK_CONTAINER(box), 3);
    gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook), box, label);
    gtk_widget_show(label);
    gtk_widget_show(box);

    label = make_bold_label(_("Resources that conflict with this package:"));
    gtk_box_pack_start(GTK_BOX(box), label, FALSE, TRUE, 0);
    gtk_widget_show(label);

    swin = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(swin),
					GTK_SHADOW_IN);
    gtk_box_pack_start(GTK_BOX(box), swin, TRUE, TRUE, 0);
    gtk_widget_show(swin);

    self->conflicts = gtk_tree_store_new(4, G_TYPE_STRING, G_TYPE_STRING,
					G_TYPE_STRING, G_TYPE_BOOLEAN);
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(self->conflicts),
					 RESOURCE_COLUMN, GTK_SORT_ASCENDING);
    g_object_set_data(G_OBJECT(self->conflicts), "match_type",
		      GINT_TO_POINTER(RPMTAG_PROVIDENAME));
    g_object_set_data(G_OBJECT(self->conflicts), "want_matches",
		      GINT_TO_POINTER(FALSE));

    tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(self->conflicts));
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(tree),
						-1, _("Resource"),
						gtk_cell_renderer_text_new(),
						"text", RESOURCE_COLUMN,
						"foreground", COLOUR_COLUMN,
						NULL);
    gtk_tree_view_column_set_sort_column_id (
		gtk_tree_view_get_column (GTK_TREE_VIEW (tree), 0),
		RESOURCE_COLUMN);
    gtk_tree_view_set_enable_search(GTK_TREE_VIEW(tree), TRUE);
    gtk_tree_view_set_search_column(GTK_TREE_VIEW(tree), RESOURCE_COLUMN);
    g_signal_connect_object(tree, "row_activated",
			    G_CALLBACK(row_activated_cb), self, 0);
    g_signal_connect(tree, "test_expand_row",
		     G_CALLBACK(resource_test_expand_row), NULL);

    gtk_container_add(GTK_CONTAINER(swin), tree);
    gtk_widget_show(tree);

    label = gtk_label_new(_("Obsoletes"));
    box = gtk_vbox_new(FALSE, 5);
    gtk_container_set_border_width(GTK_CONTAINER(box), 3);
    gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook), box, label);
    gtk_widget_show(label);
    gtk_widget_show(box);

    label = make_bold_label(_("Resources that are obsoleted by this package:"));
    gtk_box_pack_start(GTK_BOX(box), label, FALSE, TRUE, 0);
    gtk_widget_show(label);

    swin = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(swin),
					GTK_SHADOW_IN);
    gtk_box_pack_start(GTK_BOX(box), swin, TRUE, TRUE, 0);
    gtk_widget_show(swin);

    self->obsoletes = gtk_tree_store_new(4, G_TYPE_STRING, G_TYPE_STRING,
					 G_TYPE_STRING, G_TYPE_BOOLEAN);
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(self->obsoletes),
					 RESOURCE_COLUMN, GTK_SORT_ASCENDING);
    g_object_set_data(G_OBJECT(self->obsoletes), "match_type",
		      GINT_TO_POINTER(RPMTAG_NAME));
    g_object_set_data(G_OBJECT(self->obsoletes), "want_matches",
		      GINT_TO_POINTER(FALSE));

    tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(self->obsoletes));
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(tree),
						-1, _("Resource"),
						gtk_cell_renderer_text_new(),
						"text", RESOURCE_COLUMN,
						"foreground", COLOUR_COLUMN,
						NULL);
    gtk_tree_view_column_set_sort_column_id (
		gtk_tree_view_get_column (GTK_TREE_VIEW (tree), 0),
		RESOURCE_COLUMN);
    gtk_tree_view_set_enable_search(GTK_TREE_VIEW(tree), TRUE);
    gtk_tree_view_set_search_column(GTK_TREE_VIEW(tree), RESOURCE_COLUMN);
    g_signal_connect_object(tree, "row_activated",
			    G_CALLBACK(row_activated_cb), self, 0);
    g_signal_connect(tree, "test_expand_row",
		     G_CALLBACK(resource_test_expand_row), NULL);

    gtk_container_add(GTK_CONTAINER(swin), tree);
    gtk_widget_show(tree);

    gtk_widget_pop_composite_child();

    gtk_widget_set_sensitive(GTK_WIDGET(self), FALSE);
}

GtkWidget *
rpm_info_new(void)
{
    RpmInfo *self;

    self = g_object_new(RPM_TYPE_INFO, NULL);

    return GTK_WIDGET(self);
}

void
rpm_info_clear(RpmInfo *self)
{
    GtkTextIter start, end;
    gtk_image_set_from_pixbuf(GTK_IMAGE(self->icon), NULL);
    gtk_label_set_label(GTK_LABEL(self->name), "");
    gtk_label_set_label(GTK_LABEL(self->summary), "");
    gtk_label_set_label(GTK_LABEL(self->group), "");
    gtk_label_set_label(GTK_LABEL(self->size), "");
    gtk_label_set_label(GTK_LABEL(self->vendor), "");
    gtk_label_set_label(GTK_LABEL(self->packager), "");
    gtk_label_set_label(GTK_LABEL(self->url), "");
    gtk_label_set_label(GTK_LABEL(self->installdate), "");

    gtk_text_buffer_get_bounds(self->description, &start, &end);
    gtk_text_buffer_delete(self->description, &start, &end);

    gtk_text_buffer_get_bounds(self->changelog, &start, &end);
    gtk_text_buffer_delete(self->changelog, &start, &end);

    gtk_tree_store_clear(self->files);
    gtk_tree_store_clear(self->requires);
    gtk_tree_store_clear(self->provides);
    gtk_tree_store_clear(self->conflicts);
    gtk_tree_store_clear(self->obsoletes);

    gtk_notebook_set_current_page(GTK_NOTEBOOK(self->notebook), 0);

    gtk_widget_set_sensitive(GTK_WIDGET(self), FALSE);
}

static void
rpm_info_add_matches (GtkTreeStore *store, GtkTreeIter *resiter,
		      rpmdbMatchIterator mi)
{
    Header header;

    while ((header = rpmdbNextIterator(mi)) != NULL) {
	GtkTreeIter pkgiter;
	const gchar *name, *version, *release;
	gchar *group, *value, *uri;
		
	gtk_tree_store_append(store, &pkgiter, resiter);

	rpmHeaderGetEntry(header, RPMTAG_NAME, NULL, (void **)&name, NULL);
	rpmHeaderGetEntry(header, RPMTAG_VERSION,NULL, (void **)&version,NULL);
	rpmHeaderGetEntry(header, RPMTAG_RELEASE,NULL, (void **)&release,NULL);
	rpmHeaderGetEntry(header, RPMTAG_GROUP, NULL, (void **)&group, NULL);

	value = g_strdup_printf("%s-%s-%s", name, version, release);
	gtk_tree_store_set(store, &pkgiter,
			   RESOURCE_COLUMN, value,
			   -1);
	g_free(value);

	g_strchomp(group);
	value = g_strconcat("rpmdb:///", group, "/",
			    name, "-", version, "-", release, NULL);
	uri = gnome_vfs_escape_host_and_path_string(value);
	g_free(value);
	gtk_tree_store_set(store, &pkgiter,
			   URI_COLUMN, uri,
			   -1);
	g_free(uri);
    }
}

static gboolean
resource_test_expand_row(GtkTreeView *view, GtkTreeIter *iter,
			 GtkTreePath *path)
{
    GtkTreeModel *model;
    GtkTreeStore *store;
    GtkTreeIter childiter;
    gboolean has_expanded, want_matches, has_matches;
    gchar *resource, *space;
    rpmdb db = NULL;
    rpmdbMatchIterator mi;
    rpmTag match_type;

    model = gtk_tree_view_get_model(view);
    store = GTK_TREE_STORE(model);
    gtk_tree_model_get(model, iter, HAS_EXPANDED_COLUMN, &has_expanded, -1);

    if (has_expanded)
	return FALSE; /* allow expand */

    if (rpmdbOpen(NULL, &db, O_RDONLY, 0644)) {
	fprintf(stderr, "could not open RPM database\n");
	return TRUE; /* disallow database open */
    }
    /* remove dummy iter */
    gtk_tree_model_iter_children(model, &childiter, iter);
    gtk_tree_store_remove(store, &childiter);

    match_type = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(model),
						   "match_type"));
    want_matches = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(model),
						     "want_matches"));

    gtk_tree_model_get(model, iter, RESOURCE_COLUMN, &resource, -1);
    /* trim off everything after space ... */
    space = strchr(resource, ' ');
    if (space) *space = '\0';

    mi = rpmdbInitIterator(db, match_type, resource, 0);
    if (mi) {
	rpm_info_add_matches(store, iter, mi);
	rpmdbFreeIterator(mi);
    }
    /* only do filename lookups if it looks like we have a filename */
    if (resource[0] == '/') {
	mi = rpmdbInitIterator(db, RPMTAG_BASENAMES, resource, 0);
	if (mi) {
	    rpm_info_add_matches(store, iter, mi);
	    rpmdbFreeIterator(mi);
	}
    }
    g_free(resource);
    rpmdbClose(db);

    /* if no matches for the requirement are found, make it red ... */
    has_matches = gtk_tree_model_iter_has_child(model, iter);
    if ((has_matches && !want_matches) || (!has_matches && want_matches))
	gtk_tree_store_set(store, iter,
			   COLOUR_COLUMN, "red",
			   -1);

    gtk_tree_store_set(store, iter,
		       HAS_EXPANDED_COLUMN, TRUE,
		       -1);

    return !has_matches; /* allow expand if there are matches */
}

static void
rpm_info_fill_resources(GtkTreeStore *store,
			gchar **resources, gint32 *flags, gchar **versions,
			gint len, rpmTag match_type, gboolean want_matches)
{
    gint i;

    for (i = 0; i < len; i++) {
	GtkTreeIter resiter, dummyiter;

	gtk_tree_store_append(store, &resiter, NULL);
	if (flags[i]) {
	    gchar *resource;

	    resource = g_strconcat(resources[i], " ",
				   (flags[i] & RPMSENSE_LESS) ? "<" : "",
				   (flags[i] & RPMSENSE_GREATER) ? ">" : "",
				   (flags[i] & RPMSENSE_EQUAL) ? "=" : "",
				   (flags[i] & RPMSENSE_SERIAL) ? "S" : "",
				   " ", versions[i], NULL);
	    gtk_tree_store_set(store, &resiter,
			       RESOURCE_COLUMN, resource,
			       -1);
	    g_free(resource);
	} else {
	    gtk_tree_store_set(store, &resiter,
			       RESOURCE_COLUMN, resources[i],
			       -1);
	}
	/* add the fake node, so that we can trap test_expand_row */
	gtk_tree_store_set(store, &resiter,
			   HAS_EXPANDED_COLUMN, FALSE,
			   -1);
	gtk_tree_store_append(store, &dummyiter, &resiter);
    }
}

void
rpm_info_set_header (RpmInfo *self, Header header)
{
    GtkWidget *widget;
    const gchar *name, *version, *release;
    gchar *value, *description;
    gint len, res, i;
    gint32 *intval;
    gchar **dirnames, **basenames, **links, **names, **texts;
    gchar **resources, **versions;
    gint32 *dirindices, *flags;
    GtkTextIter textiter;

    rpm_info_clear(self);

    rpmHeaderGetEntry(header, RPMTAG_NAME, NULL, (void **)&name, NULL);
    rpmHeaderGetEntry(header, RPMTAG_VERSION, NULL, (void **)&version, NULL);
    rpmHeaderGetEntry(header, RPMTAG_RELEASE, NULL, (void **)&release, NULL);
    value = g_strdup_printf("<big><b>%s-%s-%s</b></big>",
			    name, version, release);
    gtk_label_set_label(GTK_LABEL(self->name), value);
    g_free(value);

    /* load up pixbuf data */
    res = rpmHeaderGetEntry(header, RPMTAG_ICON, NULL, (void **)&value, &len);
    if (value == NULL)
	res = rpmHeaderGetEntry(header, RPMTAG_GIF, NULL,
				(void **)&value, &len);
    if (value == NULL)
	res = rpmHeaderGetEntry(header, RPMTAG_XPM, NULL,
				(void **)&value, &len);
    if (value != NULL) {
	GdkPixbufLoader *loader = gdk_pixbuf_loader_new();

	/* wrote data successfully */
	if (gdk_pixbuf_loader_write(loader, value, len, NULL) &&
	    gdk_pixbuf_loader_close(loader, NULL)) {
	    GdkPixbuf *pixbuf = gdk_pixbuf_loader_get_pixbuf(loader);
	    
	    gtk_image_set_from_pixbuf(GTK_IMAGE(self->icon), pixbuf);
	}
	g_object_unref(loader);
    }

    rpmHeaderGetEntry(header, RPMTAG_SUMMARY, NULL, (void **)&value, NULL);
    g_strchomp(value);
    gtk_label_set_label(GTK_LABEL(self->summary), value);

    rpmHeaderGetEntry(header, RPMTAG_GROUP, NULL, (void **)&value, NULL);
    g_strchomp(value);
    gtk_label_set_label(GTK_LABEL(self->group), value);

    rpmHeaderGetEntry(header, RPMTAG_SIZE, NULL, (void **)&intval, NULL);
    value = gnome_vfs_format_file_size_for_display(*intval);
    gtk_label_set_label(GTK_LABEL(self->size), value);
    g_free(value);

    rpmHeaderGetEntry(header, RPMTAG_VENDOR, NULL, (void **)&value, NULL);
    if (value) g_strchomp(value);
    gtk_label_set_label(GTK_LABEL(self->vendor), value?value:_("(none)"));

    rpmHeaderGetEntry(header, RPMTAG_PACKAGER, NULL, (void **)&value, NULL);
    if (value) g_strchomp(value);
    gtk_label_set_label(GTK_LABEL(self->packager), value?value:_("(none)"));

    rpmHeaderGetEntry(header, RPMTAG_URL, NULL, (void **)&value, NULL);
    if (value) g_strchomp(value);
    gtk_label_set_label(GTK_LABEL(self->url), value?value:_("(none)"));

    rpmHeaderGetEntry(header, RPMTAG_INSTALLTIME, NULL, (void **)&intval,NULL);
    if (intval) {
	gchar buf[100];
	time_t tm = *intval;

	strftime(buf, sizeof(buf), "%a %b %d %I:%M:%S %Z %Y", localtime(&tm));
	gtk_label_set_label(GTK_LABEL(self->installdate), buf);
    }

    rpmHeaderGetEntry(header, RPMTAG_DESCRIPTION, NULL, (void **)&value, NULL);
    description = g_strdup(value);
    for (i = 0; description[i] != '\0'; i++) {
	gboolean rewrap = (description[i] != ' ');

	while (description[i] != '\n' && description[i] != '\0') i++;
	if (description[i] == '\n') {
	    if (rewrap && description[i+1] != '\n')
		description[i] = ' ';
	}
	i++;
    }
    gtk_text_buffer_set_text(self->description, description, -1);
    g_free(description);

    rpmHeaderGetEntry(header, RPMTAG_DIRNAMES, NULL,
		      (void **)&dirnames, NULL);
    rpmHeaderGetEntry(header, RPMTAG_DIRINDEXES, NULL,
		      (void **)&dirindices, NULL);
    rpmHeaderGetEntry(header, RPMTAG_BASENAMES, NULL,
		      (void **)&basenames, &len);
    rpmHeaderGetEntry(header, RPMTAG_FILELINKTOS, NULL,
		      (void **)&links, &len);

    for (i = 0; i < len; i++) {
	GtkTreeIter iter;
	gchar *filename, *uri;

	gtk_tree_store_append(self->files, &iter, NULL);
	if (links[i][0] != '\0')
	    filename = g_strconcat(dirnames[dirindices[i]], basenames[i],
				   " -> ", links[i], NULL);
	else
	    filename = g_strconcat(dirnames[dirindices[i]], basenames[i],NULL);
	gtk_tree_store_set(self->files, &iter,
			   FILENAME_COLUMN, filename,
			   -1);
	g_free(filename);

	filename = g_strconcat(dirnames[dirindices[i]],
			       basenames[i], NULL);
	uri = g_filename_to_uri(filename, NULL, NULL);
	g_free(filename);
	gtk_tree_store_set(self->files, &iter,
			   URI_COLUMN, uri,
			   -1);
	g_free(uri);
    }

    rpmHeaderGetEntry(header, RPMTAG_CHANGELOGTIME, NULL,
		      (void **)&flags, &len);
    rpmHeaderGetEntry(header, RPMTAG_CHANGELOGNAME, NULL,
		      (void **)&names, NULL);
    rpmHeaderGetEntry(header, RPMTAG_CHANGELOGTEXT, NULL,
		      (void **)&texts, NULL);
    gtk_text_buffer_get_start_iter(self->changelog, &textiter);
    for (i = 0; i < len; i++) {
	gchar buf[100];
	time_t tm = flags[i];

	strftime(buf, sizeof(buf), "%Y-%m-%d", localtime(&tm));
	gtk_text_buffer_insert_with_tags_by_name(self->changelog, &textiter,
						 buf, -1,
						 "date", NULL);
	gtk_text_buffer_insert(self->changelog, &textiter, "  ", 2);
	gtk_text_buffer_insert_with_tags_by_name(self->changelog, &textiter,
						 names[i], -1,
						 "name", NULL);
	gtk_text_buffer_insert(self->changelog, &textiter, "\n", 1);
	gtk_text_buffer_insert_with_tags_by_name(self->changelog, &textiter,
						 texts[i], -1,
						 "text", NULL);
	gtk_text_buffer_insert(self->changelog, &textiter, "\n\n", 2);
    }
    widget = gtk_notebook_get_nth_page(GTK_NOTEBOOK(self->notebook),
				       CHANGELOG_PAGE);
    if (len > 0)
	gtk_widget_show(widget);
    else
	gtk_widget_hide(widget);


    rpmHeaderGetEntry(header, RPMTAG_REQUIRENAME, NULL,
		      (void **)&resources, &len);
    rpmHeaderGetEntry(header, RPMTAG_REQUIREFLAGS, NULL,
		      (void **)&flags, NULL);
    rpmHeaderGetEntry(header, RPMTAG_REQUIREVERSION, NULL,
		      (void **)&versions, NULL);
    rpm_info_fill_resources(self->requires, resources, flags, versions, len,
			    RPMTAG_PROVIDENAME, TRUE);
    widget = gtk_notebook_get_nth_page(GTK_NOTEBOOK(self->notebook),
				       REQUIRES_PAGE);
    if (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(self->requires), NULL))
	gtk_widget_show(widget);
    else
	gtk_widget_hide(widget);

    rpmHeaderGetEntry(header, RPMTAG_PROVIDENAME, NULL,
		      (void **)&resources, &len);
    rpmHeaderGetEntry(header, RPMTAG_PROVIDEFLAGS, NULL,
		      (void **)&flags, NULL);
    rpmHeaderGetEntry(header, RPMTAG_PROVIDEVERSION, NULL,
		      (void **)&versions, NULL);
    rpm_info_fill_resources(self->provides, resources, flags, versions, len,
			    RPMTAG_REQUIRENAME, TRUE);
    widget = gtk_notebook_get_nth_page(GTK_NOTEBOOK(self->notebook),
				       PROVIDES_PAGE);
    if (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(self->provides), NULL))
	gtk_widget_show(widget);
    else
	gtk_widget_hide(widget);

    rpmHeaderGetEntry(header, RPMTAG_CONFLICTNAME, NULL,
		      (void **)&resources, &len);
    rpmHeaderGetEntry(header, RPMTAG_CONFLICTFLAGS, NULL,
		      (void **)&flags, NULL);
    rpmHeaderGetEntry(header, RPMTAG_CONFLICTVERSION, NULL,
		      (void **)&versions, NULL);
    rpm_info_fill_resources(self->conflicts, resources, flags, versions, len,
			    RPMTAG_PROVIDENAME, FALSE);
    widget = gtk_notebook_get_nth_page(GTK_NOTEBOOK(self->notebook),
				       CONFLICTS_PAGE);
    if (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(self->conflicts), NULL))
	gtk_widget_show(widget);
    else
	gtk_widget_hide(widget);

    rpmHeaderGetEntry(header, RPMTAG_OBSOLETENAME, NULL,
		      (void **)&resources, &len);
    rpmHeaderGetEntry(header, RPMTAG_OBSOLETEFLAGS, NULL,
		      (void **)&flags, NULL);
    rpmHeaderGetEntry(header, RPMTAG_OBSOLETEVERSION, NULL,
		      (void **)&versions, NULL);
    rpm_info_fill_resources(self->obsoletes, resources, flags, versions, len,
			    RPMTAG_NAME, FALSE);
    widget = gtk_notebook_get_nth_page(GTK_NOTEBOOK(self->notebook),
				       OBSOLETES_PAGE);
    if (gtk_tree_model_iter_n_children(GTK_TREE_MODEL(self->obsoletes), NULL))
	gtk_widget_show(widget);
    else
	gtk_widget_hide(widget);

    gtk_widget_set_sensitive(GTK_WIDGET(self), TRUE);
}

static void
row_activated_cb(GtkTreeView *tree_view, GtkTreePath *path,
		 GtkTreeViewColumn *column, RpmInfo *self)
{
    GtkTreeModel *model;
    GtkTreeIter iter;
    gchar *uri;

    model = gtk_tree_view_get_model(tree_view);

    gtk_tree_model_get_iter(model, &iter, path);
    gtk_tree_model_get(model, &iter,
		       URI_COLUMN, &uri,
		       -1);

    if (uri) {
	g_signal_emit(self, rpm_info_signals[ACTIVATE_URI], 0, uri);
	g_free(uri);
    } else {
	/* toggle expanded state for row ... */
	if (gtk_tree_view_row_expanded(tree_view, path))
	    gtk_tree_view_collapse_row(tree_view, path);
	else
	    gtk_tree_view_expand_row(tree_view, path, FALSE);
    }
}

static void
rpm_info_destroy(GtkObject *object)
{
    RpmInfo *self;

    self = RPM_INFO(object);

    /* NULL out these fields, which point to freed widgets after destruction */
    self->name = NULL;
    self->notebook = NULL;
    self->icon = NULL;
    self->summary = NULL;
    self->group = NULL;
    self->size = NULL;
    self->vendor = NULL;
    self->packager = NULL;
    self->url = NULL;
    self->installdate = NULL;
    self->description = NULL;
    self->files = NULL;
    self->requires = NULL;
    self->provides = NULL;
    self->conflicts = NULL;
    self->obsoletes = NULL;

    if (GTK_OBJECT_CLASS(parent_class)->destroy)
	(* GTK_OBJECT_CLASS(parent_class)->destroy)(object);
}
