/* -*- mode: C; c-basic-offset: 4 -*- */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/* needed for rpmdb.h to parse correctly */
#define _BSD_SOURCE

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <sys/types.h>

#include <rpmlib.h>
#include <rpmdb.h>

#include <glib.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-module.h>

#define RPM_PACKAGE_MIME_TYPE "application/x-installed-rpm"
#define RPM_GROUP_MIME_TYPE "x-directory/normal"

G_LOCK_DEFINE_STATIC (rpmdb_lock);
static rpmdb db = NULL;
static guint db_usage_count = 0;

static gboolean
db_open(void)
{
    G_LOCK(rpmdb_lock);

    if (db_usage_count == 0) {
	if (rpmdbOpen (NULL, &db, O_RDONLY, 0644)) {
	    g_warning ("could not open rpm database");
	    G_UNLOCK (rpmdb_lock);
	    return FALSE;
	}
    }
    db_usage_count++;

    G_UNLOCK(rpmdb_lock);

    return TRUE;
}

static void
db_close(void)
{
    G_LOCK(rpmdb_lock);

    db_usage_count--;

    if (db_usage_count == 0) {
	rpmdbClose(db);
	db = NULL;
    }

    G_UNLOCK(rpmdb_lock);
}

typedef struct _PackageGroup PackageGroup;
struct _PackageGroup
{
    PackageGroup *parent;
    GList *children;

    gchar *name;
    const gchar *shortname;
    gboolean current;
};

static PackageGroup *root_group = NULL;

static int
package_group_compare(gconstpointer a, gconstpointer b)
{
    const PackageGroup *pa, *pb;

    pa = (const PackageGroup *)a;
    pb = (const PackageGroup *)b;

    return strcmp(pa->shortname, pb->shortname);
}

static PackageGroup *
get_package_group (const gchar *name, gboolean create)
{
    const gchar *component;
    PackageGroup *parent, *package;
    GList *tmp;

    if (name && name[0] == '/')
	name++;
    if (name == NULL || name[0] == '\0') {
	if (!root_group) {
	    root_group = g_new(PackageGroup, 1);
	    root_group->parent = NULL;
	    root_group->children = NULL;
	    root_group->name = g_strdup("Root");
	    root_group->shortname = root_group->name;
	    root_group->current = TRUE;
	}
	return root_group;
    }

    /* get parent node */
    component = strrchr(name, '/');
    if (component) {
	gchar *parent_name = g_strndup(name, component - name);

	parent = get_package_group(parent_name, create);
	component++;
    } else {
	parent = get_package_group(NULL, TRUE);
	component = name;
    }

    if (!parent) return NULL;

    for (tmp = parent->children; tmp != NULL; tmp = tmp->next) {
	PackageGroup *child = tmp->data;

	if (!strcmp(child->shortname, component)) {
	    if (create)
		child->current = TRUE;
	    return child;
	}
    }

    if (!create)
	return NULL;

    package = g_new(PackageGroup, 1);
    package->parent = parent;
    package->children = NULL;
    package->name = g_strdup(name);
    component = strrchr(package->name, '/');
    if (component)
	package->shortname = component+1;
    else
	package->shortname = package->name;
    package->current = TRUE;
    parent->children = g_list_insert_sorted(parent->children, package,
					    package_group_compare);

    return package;
}

static void
mark_unused (PackageGroup *package)
{
    GList *tmp;

    package->current = FALSE;
    for (tmp = package->children; tmp != NULL; tmp = tmp->next) {
	PackageGroup *child = tmp->data;

	mark_unused(child);
    }
}
static void
reap_unused (PackageGroup *package)
{
    GList *tmp;

    for (tmp = package->children; tmp != NULL; tmp = tmp->next) {
	PackageGroup *child = tmp->data;

	reap_unused(child);
    }

    if (!package->current && package != root_group) {
	g_free(package->name);
	package->parent->children = g_list_remove(package->parent->children,
						  package);
	g_free(package);
    }
}

static void
update_package_groups (void)
{
    rpmdbMatchIterator mi;
    Header header;

    if (!db_open()) return;

    /* mark all package group nodes as unused */
    if (root_group) {
	mark_unused(root_group);
    }
    /* make sure root package group is available */
    get_package_group(NULL, TRUE);

    /* create all package group nodes */
    mi = rpmdbInitIterator(db, RPMDBI_PACKAGES, NULL, 0);
    while ((header = rpmdbNextIterator(mi)) != NULL) {
	const gchar *group;

	if (headerGetEntry(header, RPMTAG_GROUP, NULL, (void **)&group, NULL))
	    get_package_group(group, TRUE);
    }
    rpmdbFreeIterator(mi);

    /* remove nodes that are still marked as unused. */
    reap_unused(root_group);

    db_close();
}

static gchar *
get_path_from_uri (GnomeVFSURI const *uri)
{
    gchar *path;
    gint len;

    path = gnome_vfs_unescape_string (uri->text, 
				      G_DIR_SEPARATOR_S);
                
    if (path == NULL) {
	return NULL;
    }

    if (path[0] != G_DIR_SEPARATOR) {
	g_free (path);
	return NULL;
    }

    len = strlen(path);
    if (path[len-1] == G_DIR_SEPARATOR) path[len-1] = '\0';

    return path;
}

static void
fill_from_header (GnomeVFSFileInfo *file_info, Header header)
{
    char *name, *version, *release;
    gint32 *size, *mytime;

    /* package matches */
    headerGetEntry(header, RPMTAG_NAME, NULL, (void **)&name, NULL);
    headerGetEntry(header, RPMTAG_VERSION, NULL, (void **)&version, NULL);
    headerGetEntry(header, RPMTAG_RELEASE, NULL, (void **)&release, NULL);

    g_free(file_info->name);
    file_info->name = g_strdup_printf("%s-%s-%s", name, version, release);

    file_info->type = GNOME_VFS_FILE_TYPE_REGULAR;
    file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_TYPE;

    headerGetEntry(header, RPMTAG_SIZE, NULL, (void **)&size, NULL);
    file_info->size = size[0];
    file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_SIZE;

    headerGetEntry(header, RPMTAG_BUILDTIME, NULL, (void **)&mytime, NULL);
    if (mytime) {
	file_info->ctime = mytime[0];
	file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_CTIME;
    }
    headerGetEntry(header, RPMTAG_INSTALLTIME, NULL, (void **)&mytime, NULL);
    if (mytime) {
	file_info->mtime = mytime[0];
	file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_MTIME;
    }

    g_free(file_info->mime_type);
    file_info->mime_type = g_strdup(RPM_PACKAGE_MIME_TYPE);
    file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;
}

static void
fill_from_package_group(GnomeVFSFileInfo *file_info, PackageGroup *group)
{
    g_free(file_info->name);
    file_info->name = g_strdup(group->shortname);

    file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
    file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_TYPE;

    g_free(file_info->mime_type);
    file_info->mime_type = g_strdup(RPM_GROUP_MIME_TYPE);
    file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;
}

typedef struct _GroupListHandle GroupListHandle;
struct _GroupListHandle {
    enum { POS_ALL, POS_GROUPS, POS_PACKAGES, POS_END } position;
    PackageGroup *group;
    GList *nextchild;
    rpmdbMatchIterator mi;
};

static GnomeVFSResult
do_open_directory (GnomeVFSMethod *method,
                   GnomeVFSMethodHandle **method_handle,
                   GnomeVFSURI *uri,
                   GnomeVFSFileInfoOptions options,
                   GnomeVFSContext *context)
{
    GnomeVFSResult result = GNOME_VFS_ERROR_NOT_SUPPORTED;
    GroupListHandle *handle;
    char *path = NULL;    
    PackageGroup *group;

    path = get_path_from_uri (uri);
    if (!path)
	return GNOME_VFS_ERROR_INVALID_URI;

    /* special "/All" directory */
    if (!strcmp(path, "/All")) {
	if (!db_open()) {
	    result = GNOME_VFS_ERROR_INTERNAL;
	    goto end;
	}

	handle = g_new(GroupListHandle, 1);

	handle->position = POS_PACKAGES;
	handle->group = NULL;
	handle->nextchild = NULL;
	handle->mi = rpmdbInitIterator(db, RPMDBI_PACKAGES, NULL, 0);
	*method_handle = (GnomeVFSMethodHandle *)handle;

	result = GNOME_VFS_OK;
	goto end;
    }

    group = get_package_group(path, FALSE);
    if (!group) {
	result = GNOME_VFS_ERROR_NOT_FOUND;
	goto end;
    }
    
    if (!db_open()) {
	result = GNOME_VFS_ERROR_INTERNAL;
	goto end;
    }
    handle = g_new(GroupListHandle, 1);
    handle->group = group;
    handle->nextchild = group->children;
    handle->mi = rpmdbInitIterator(db, RPMTAG_GROUP, group->name, 0);

    if (group == root_group) {
	handle->position = POS_ALL;
    } else if (handle->nextchild) {
	handle->position = POS_GROUPS;
    } else if (handle->mi) {
	handle->position = POS_PACKAGES;
    } else {
	handle->position = POS_END;
    }
    *method_handle = (GnomeVFSMethodHandle *)handle;

    result = GNOME_VFS_OK;

 end:
    g_free(path);
    return result;
}

static GnomeVFSResult
do_close_directory (GnomeVFSMethod *method,
                    GnomeVFSMethodHandle *method_handle,
                    GnomeVFSContext *context)
{
    GroupListHandle *handle;

    handle = (GroupListHandle *)method_handle;

    if (handle->mi)
	rpmdbFreeIterator(handle->mi);
    handle->mi = NULL;
    handle->nextchild = NULL;
    handle->group = NULL;
    db_close();
    g_free(handle);
    
    return GNOME_VFS_OK;
}

static GnomeVFSResult
do_read_directory (GnomeVFSMethod *method,
                   GnomeVFSMethodHandle *method_handle,
                   GnomeVFSFileInfo *file_info,
                   GnomeVFSContext *context)
{
    GnomeVFSResult result = GNOME_VFS_ERROR_NOT_SUPPORTED;
    GroupListHandle *handle;
    Header header;

    handle = (GroupListHandle *)method_handle;
    switch (handle->position) {
    case POS_ALL:
	g_free(file_info->name);
	file_info->name = g_strdup("All");

	file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
	file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_TYPE;

	g_free(file_info->mime_type);
	file_info->mime_type = g_strdup(RPM_GROUP_MIME_TYPE);
	file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;

	/* move handle on to next type */
	if (handle->nextchild) {
	    handle->position = POS_GROUPS;
	} else if (handle->mi) {
	    handle->position = POS_PACKAGES;
	} else {
	    handle->position = POS_END;
	}

	result = GNOME_VFS_OK;
	break;

    case POS_GROUPS:
	fill_from_package_group(file_info,
				(PackageGroup *)handle->nextchild->data);
	handle->nextchild = handle->nextchild->next;

	/* move handle on to next type */
	if (handle->nextchild) {
	    handle->position = POS_GROUPS;
	} else if (handle->mi) {
	    handle->position = POS_PACKAGES;
	} else {
	    handle->position = POS_END;
	}

	result = GNOME_VFS_OK;
	break;

    case POS_PACKAGES:
	header = rpmdbNextIterator(handle->mi);
	if (header) {
	    fill_from_header(file_info, header);
	    result = GNOME_VFS_OK;
	} else {
	    /* exhausted packages */
	    rpmdbFreeIterator(handle->mi);
	    handle->mi = NULL;
	    handle->position = POS_END;
	    result = GNOME_VFS_ERROR_EOF;
	}
	break;

    case POS_END:
	result = GNOME_VFS_ERROR_EOF;
	break;
    }

    return result;
}

static GnomeVFSResult
do_open (GnomeVFSMethod *method,
         GnomeVFSMethodHandle **method_handle,
         GnomeVFSURI *uri,
         GnomeVFSOpenMode mode,
         GnomeVFSContext *context)
{
    return GNOME_VFS_ERROR_NOT_SUPPORTED;
}

static gboolean
do_is_local(GnomeVFSMethod *method,
	    const GnomeVFSURI *uri)
{
    return TRUE;
}

static GnomeVFSResult
do_get_file_info (GnomeVFSMethod *method,
                  GnomeVFSURI *uri,
                  GnomeVFSFileInfo *file_info,
                  GnomeVFSFileInfoOptions options,
                  GnomeVFSContext *context)
{
    GnomeVFSResult result = GNOME_VFS_ERROR_NOT_SUPPORTED;
    char *path = NULL, *part, *val;
    const char *groupname;
    PackageGroup *group;
    rpmdbMatchIterator mi;
    Header header;

    path = get_path_from_uri (uri);
    if (!path)
	return GNOME_VFS_ERROR_INVALID_URI;

    if (!strcmp(path, "/All")) {
	g_free(file_info->name);
	file_info->name = g_strdup("All");

	file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
	file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_TYPE;

	g_free(file_info->mime_type);
	file_info->mime_type = g_strdup(RPM_GROUP_MIME_TYPE);
	file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;

	result = GNOME_VFS_OK;
	goto end;
    }

    group = get_package_group(path, FALSE);
    if (group) {
	fill_from_package_group(file_info, group);
	result = GNOME_VFS_OK;
	goto end;
    }

    part = strrchr(path, '/');
    if (!part) {
	result = GNOME_VFS_ERROR_NOT_FOUND;
	goto end;
    }

    /* is of form group/name-version-release */
    part[0] = '\0';
    part++;
    groupname = path;
    if (groupname[0] == '/') groupname++;

    if (!db_open()) {
	result = GNOME_VFS_ERROR_INTERNAL;
	goto end;
    }
    mi = rpmdbInitIterator(db, RPMDBI_LABEL, part, 0);
    if (!mi) {
	db_close();
	result = GNOME_VFS_ERROR_NOT_FOUND;
	goto end;
    }
    header = rpmdbNextIterator(mi);
    headerGetEntry(header, RPMTAG_GROUP, NULL, (void **)&val, NULL);
    /* group doesn't match, and we aren't in the "All" directory. */
    if (strcmp(groupname, "All") != 0 && strcmp(groupname, val) != 0) {
	rpmdbFreeIterator(mi);
	db_close();
	result = GNOME_VFS_ERROR_NOT_FOUND;
	goto end;
    }

    fill_from_header(file_info, header);
    rpmdbFreeIterator(mi);
    db_close();

    result = GNOME_VFS_OK;

 end:
    g_free(path);
    return result;
}

static GnomeVFSMethod method = {
    sizeof (GnomeVFSMethod),

    .open_directory = do_open_directory,
    .close_directory = do_close_directory,
    .read_directory = do_read_directory,

    .open = do_open,
    .is_local = do_is_local,
    .get_file_info = do_get_file_info,
};

GnomeVFSMethod *
vfs_module_init (const char *method_name, const char *args)
{
    rpmReadConfigFiles(NULL, NULL);
    update_package_groups();

    if (!strcmp(method_name, "rpmdb"))
        return &method;
    return NULL;
}

void
vfs_module_shutdown (GnomeVFSMethod *method)
{
}
