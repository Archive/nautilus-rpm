/* -*- mode: C; c-basic-offset: 4 -*- */
#ifndef NAUTILUS_RPM_VIEW_H
#define NAUTILUS_RPM_VIEW_H

#include <libnautilus/nautilus-view.h>

#define NAUTILUS_TYPE_RPM_VIEW            (nautilus_rpm_view_get_type ())
#define NAUTILUS_RPM_VIEW(obj)            (GTK_CHECK_CAST ((obj), NAUTILUS_TYPE_RPM_VIEW, NautilusRpmView))
#define NAUTILUS_RPM_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), NAUTILUS_TYPE_RPM_VIEW, NautilusRpmViewClass))
#define NAUTILUS_IS_RPM_VIEW(obj)         (GTK_CHECK_TYPE ((obj), NAUTILUS_TYPE_RPM_VIEW))
#define NAUTILUS_IS_RPM_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), NAUTILUS_TYPE_RPM_VIEW))

typedef struct _NautilusRpmView      NautilusRpmView;
typedef struct _NautilusRpmViewClass NautilusRpmViewClass;

struct _NautilusRpmView {
    NautilusView parent;

    GtkWidget *rpminfo;
};

struct _NautilusRpmViewClass {
    NautilusViewClass parent_class;
};

GType nautilus_rpm_view_get_type (void);

#endif
