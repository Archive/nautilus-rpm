/* -*- mode: C; c-basic-offset: 4 -*- */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/* needed for rpmdb.h to parse correctly */
#define _BSD_SOURCE

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <rpmlib.h>
#include <rpmdb.h>
#ifdef HAVE_RPM_4_1
#  include <rpmts.h>
#endif

#include "nautilus-rpm-view.h"
#include "rpm-info.h"
#include <libgnomevfs/gnome-vfs.h>
#include <libnautilus/nautilus-view-standard-main.h>

static void nautilus_rpm_view_init (NautilusRpmView *self);
static void nautilus_rpm_view_class_init (NautilusRpmViewClass *class);

static void rpm_view_load_location (NautilusView *view, const gchar *location);
static void rpm_view_activate_uri (RpmInfo *rpminfo, const gchar *uri,
				   NautilusView *self);

GType
nautilus_rpm_view_get_type(void)
{
    static GType object_type = 0;

    if (!object_type) {
        static const GTypeInfo object_info = {
            sizeof(NautilusRpmViewClass),
            (GBaseInitFunc) NULL,
            (GBaseFinalizeFunc) NULL,
            (GClassInitFunc) nautilus_rpm_view_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof(NautilusRpmView),
            0, /* n_preallocs */
            (GInstanceInitFunc) nautilus_rpm_view_init,
        };
        object_type = g_type_register_static(NAUTILUS_TYPE_VIEW,
                                             "NautilusRpmView",
                                             &object_info, 0);
    }

    return object_type;
}

static void
nautilus_rpm_view_class_init (NautilusRpmViewClass *class)
{
}

static void
nautilus_rpm_view_init(NautilusRpmView *self)
{
    self->rpminfo = rpm_info_new();
    gtk_widget_show(self->rpminfo);

    nautilus_view_construct(NAUTILUS_VIEW(self), self->rpminfo);

    g_signal_connect_object(self->rpminfo, "activate_uri",
			    G_CALLBACK(rpm_view_activate_uri), self, 0);
    g_signal_connect(self, "load_location",
		     G_CALLBACK(rpm_view_load_location), NULL);
}

static void
rpm_view_load_location(NautilusView *view, const gchar *location)
{
    NautilusRpmView *self;
    rpmdb db = NULL;

    self = NAUTILUS_RPM_VIEW(view);

    nautilus_view_report_load_underway (view);

    if (rpmdbOpen(NULL, &db, O_RDONLY, 0644)) {
	fprintf(stderr, "could not open RPM database\n");
	nautilus_view_report_load_failed(view);
	goto end;
    }

    
    /* is an installed rpm -- get the basename */
    if (strlen(location) > 6 && !strncmp("rpmdb:", location, 6)) {
	gchar *pkg = gnome_vfs_unescape_string(g_basename(location), "/");
	rpmdbMatchIterator mi;
	Header header;

	if (!pkg) {
	    nautilus_view_report_load_failed(view);
	    goto end;
	}
	mi = rpmdbInitIterator(db, RPMDBI_LABEL, pkg, 0);
	if (!mi) {
	    nautilus_view_report_load_failed(view);
	    goto end;
	}
	header = rpmdbNextIterator(mi);
	if (!header) {
	    rpmdbFreeIterator(mi);
	    nautilus_view_report_load_failed(view);
	    goto end;
	}
	rpm_info_set_header(RPM_INFO(self->rpminfo), header);
	rpmdbFreeIterator(mi);
    } else if (strlen(location) > 5 && !strncmp("file:", location, 5)) {
	gchar *filename = gnome_vfs_get_local_path_from_uri(location);
	FD_t fd;
	Header header = NULL;
	gint res;

	if (!filename) {
	    nautilus_view_report_load_failed(view);
	    goto end;
	}
	fd = Fopen(filename, "r");
	if (!fd) {
	    nautilus_view_report_load_failed(view);
	    g_free(filename);
	    goto end;
	}
#ifdef HAVE_RPM_4_1
	{
	    rpmts ts = rpmtsCreate();
	    res = rpmReadPackageFile(ts, fd, filename, &header);
	    rpmtsFree(ts);
	}
#else
	res = rpmReadPackageHeader(fd, &header, NULL, NULL, NULL);
#endif
	g_free(filename);
	if ((res != RPMRC_OK && res != RPMRC_NOTTRUSTED && res != RPMRC_NOKEY)|| !header) {
	    nautilus_view_report_load_failed(view);
	    if (header) headerFree(header);
	    Fclose(fd);
	    goto end;
	}
	rpm_info_set_header(RPM_INFO(self->rpminfo), header);
	headerFree(header);
	Fclose(fd);
    } else {
	nautilus_view_report_load_failed(view);
	goto end;
    }

    nautilus_view_report_load_complete (view);

 end:
    if (db)
	rpmdbClose(db);
}

static void
rpm_view_activate_uri (RpmInfo *rpminfo, const gchar *uri,
		       NautilusView *self)
{
    nautilus_view_open_location(self,
                                uri,
                                (Nautilus_ViewFrame_OpenMode)0,
                                (Nautilus_ViewFrame_OpenFlags)0,
                                NULL);
}


/* -------------------------------- */

#define VIEW_IID    "OAFIID:Nautilus_Rpm_View"
#define FACTORY_IID "OAFIID:Nautilus_Rpm_View_Factory"

int
main (int argc, char *argv[])
{
    rpmReadConfigFiles(NULL, NULL);

    return nautilus_view_standard_main ("nautilus-rpm-view", VERSION,
					GETTEXT_PACKAGE, NAUTILUSRPM_LOCALEDIR,
					argc, argv, FACTORY_IID, VIEW_IID,
					nautilus_view_create_from_get_type_function,
					NULL,
					nautilus_rpm_view_get_type);
}
