# Brazilian Portuguese translation for nautilus-rpm.
# Copyright (C) 2003 Evandro Fernandes Giovanini
# This file is distributed under the same license as the 
# nautilus-rpm package.
# Evandro Fernandes Giovanini <evandrofg@ig.com.br>, 2003.
#
msgid ""
msgstr ""
"Project-Id-Version: nautilus-rpm 0.1\n"
"POT-Creation-Date: 2003-03-04 15:50-0300\n"
"PO-Revision-Date: 2003-03-04 15:46-0300\n"
"Last-Translator: Evandro Fernandes Giovanini <evandrofg@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <gnome-l10n-br@listas.cipsga.org.br>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/rpm-info.c:148
msgid "Info"
msgstr "Info"

#: src/rpm-info.c:164
msgid "Summary:"
msgstr "Sumário:"

#: src/rpm-info.c:177 src/nautilus-rpm-properties-page.c:149
msgid "Group:"
msgstr "Grupo:"

#: src/rpm-info.c:189
msgid "Size:"
msgstr "Tamanho:"

#: src/rpm-info.c:201
msgid "Vendor:"
msgstr "Vendor:"

#: src/rpm-info.c:213
msgid "Packager:"
msgstr "Empacotador:"

#: src/rpm-info.c:225
msgid "URL:"
msgstr "URL:"

#: src/rpm-info.c:237 src/nautilus-rpm-properties-page.c:160
msgid "Install date:"
msgstr "Data de Instalação:"

#: src/rpm-info.c:265
msgid "Files"
msgstr "Arquivos"

#: src/rpm-info.c:281
msgid "File name"
msgstr "Nome do arquivo"

#: src/rpm-info.c:291
msgid "Change log"
msgstr "Log de mudanças"

#: src/rpm-info.c:316
msgid "Requires"
msgstr "Requerimentos"

#: src/rpm-info.c:323
msgid "Resources required by this package:"
msgstr "Recursos requeridos por este pacote:"

#: src/rpm-info.c:346 src/rpm-info.c:394 src/rpm-info.c:442 src/rpm-info.c:490
msgid "Resource"
msgstr "Recurso"

#: src/rpm-info.c:364
msgid "Provides"
msgstr "Provém"

#: src/rpm-info.c:371
msgid "Resources provided by this package:"
msgstr "Recursos providos por este pacote:"

#: src/rpm-info.c:412
msgid "Conflicts"
msgstr "Conflitos"

#: src/rpm-info.c:419
msgid "Resources that conflict with this package:"
msgstr "Recursos que conflitam com este pacote:"

#: src/rpm-info.c:460
msgid "Obsoletes"
msgstr "Obsoletos"

#: src/rpm-info.c:467
msgid "Resources that are obsoleted by this package:"
msgstr "Recursos que são obsoletos por este pacote:"

#: src/rpm-info.c:754 src/rpm-info.c:758 src/rpm-info.c:762
msgid "(none)"
msgstr "(nenhum)"

#: src/nautilus-rpm-properties-page.c:127
msgid "Name:"
msgstr "Nome:"

#. clear out any existing info
#: src/nautilus-rpm-properties-page.c:131
#: src/nautilus-rpm-properties-page.c:230
msgid "<none>"
msgstr "<nenhum>"

#: src/nautilus-rpm-properties-page.c:138
msgid "Version:"
msgstr "Versão:"

#: src/nautilus-rpm-properties-page.c:193
msgid "URI currently displayed"
msgstr "URI exibida atualmente"

#: src/nautilus-rpm-properties-page.c:342
msgid "Package information properties page"
msgstr "Página de propriedades de informações do pacote"

#: src/rpmdb.keys.in.h:1
msgid "Installed RPM Package"
msgstr "Pacote RPM Instalado"

#: src/rpmdb.desktop.in.h:1
msgid "Browse the set of packages installed on the system"
msgstr "Visualizar os pacotes instalados no sistema"

#: src/rpmdb.desktop.in.h:2
msgid "RPM Package Database"
msgstr "Banco de Dados de Pacotes RPM"

#: src/Nautilus_View_rpm.server.in.in.h:1
msgid "Factory for RPM properties_page"
msgstr "Fábrica para properties_page de RPM"

#: src/Nautilus_View_rpm.server.in.in.h:2
msgid "Factory for RPM view"
msgstr "Fábrica para Visualizador RPM"

#: src/Nautilus_View_rpm.server.in.in.h:3
msgid "Package"
msgstr "Pacote"

#: src/Nautilus_View_rpm.server.in.in.h:4
msgid "RPM"
msgstr "RPM"

#: src/Nautilus_View_rpm.server.in.in.h:5
msgid "RPM Information"
msgstr "Informações do RPM"

#: src/Nautilus_View_rpm.server.in.in.h:6
msgid "RPM properties page"
msgstr "Página de Propriedades RPM"

#: src/Nautilus_View_rpm.server.in.in.h:7
msgid "RPM properties page factory"
msgstr "Fábrica da página de propriedades RPM"

#: src/Nautilus_View_rpm.server.in.in.h:8
msgid "RPM view"
msgstr "Visualizador RPM"

#: src/Nautilus_View_rpm.server.in.in.h:9
msgid "RPM view factory"
msgstr "Fábrica do Visualizador RPM"

#: src/Nautilus_View_rpm.server.in.in.h:10
msgid "View as RPM"
msgstr "Ver como RPM"
